1. Clona este repositorio en tu máquina
2. Borra la carpeta .git y el archivo readme.md.
3. Inicializa un repositorio local
4. Añade al stage los archivos de los directorios 1 y 2.
5. Comprueba con git status que el stage tiene lo que quieres.
6. Crea un commit con esos dos archivos.
7. Realiza una modificación en el texto del archivo 1/1.txt y otra en el texto del archivo 2/2.txt.
8. Añade al stage únicamente las modificaciones del archivo 2/2.txt.
9. Haz git status e interpreta la información que te da.
10. Crea un commit con las modificaciones que has hecho en el archivo 2/2.txt y con el archivo 3/3.txt.
11. Haz git status y comprueba que el stage está vacío y que en el working directory está el cambio del archivo 1/1.txt.
12. Desversiona el archivo 1/1.txt (es decir, dile a Git que no lo versione), pero sin eliminarlo.
13. Elimina el archivo 2/2.txt.
14. Haz git status y comprueba que la eliminación de los dos archivos está en el stage.
15. Crea un commit que almacene la eliminación de esos dos últimos archivos.
16. Haz git status y comprueba que todo está como esperas (el archivo 1/1.txt está sin versionar y el stage está sin
cambios).
17. Configura el repositorio para que ignore la carpeta 1.
18. Haz git status y comprueba la diferencia.
